package com.construct.Domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Role implements Serializable {
	private static final long serailVesionUID = 890245234L;
	
	@Id
	private int roleid;
	
	private String name;
	
	@OneToMany(mappedBy = "role", cascade= CascadeType.ALL, fetch= FetchType.LAZY)
	private Set<UserRole> userRole = new HashSet<>();
	
	public Role() {}
	
	
	
	

	public int getRoleid() {
		return roleid;
	}

	public void setRoleid(int roleid) {
		this.roleid = roleid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<UserRole> getUserRole() {
		return userRole;
	}

	public void setUserRole(Set<UserRole> userRole) {
		this.userRole = userRole;
	}
	
	
	
}
