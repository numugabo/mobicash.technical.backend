package com.construct;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.construct.Config.SecurityUtility;
import com.construct.Domain.Role;
import com.construct.Domain.User;
import com.construct.Domain.UserRole;
import com.construct.Service.UserService;

@SpringBootApplication
@ComponentScan
@EnableAutoConfiguration
public class ConstructionApplication implements CommandLineRunner{
	
	@Autowired
	private UserService userService;

	public static void main(String[] args) {
		SpringApplication.run(ConstructionApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	@Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
				registry
				.addMapping("/product/**")
				.allowedOrigins("*")  // for /** means all mapping URL, and * for all domain
				.allowedMethods("GET", "POST", "PUT", "DELETE")
				.allowedHeaders("header1", "header2", "header3")
		        .exposedHeaders("header1", "header2")
				.allowCredentials(false).
				maxAge(3600);
            }
        };
    }

//	@Override
//	public void run(String... args) throws Exception {
//		User user1 = new User();
//		user1.setFirstName("numugabo");
//		user1.setLastName("Jean de Dieu");
//		user1.setUsername("jando");
//		user1.setPassword(SecurityUtility.passwordEncoder().encode("password"));
//		user1.setEmail("numugaboj@gmail.com");
//		Set<UserRole> userRoles=new HashSet<>();
//		Role role1=new Role();
//		role1.setRoleid(0);
//		role1.setName("ROLE_ADMIN");
//		userRoles.add(new UserRole(user1,role1));
//		userService.createUser(user1, userRoles);
//		userRoles.clear();
//		
//		User user2 = new User();
//		user2.setFirstName("numugabo");
//		user2.setLastName("Didier");
//		user2.setUsername("numudidi");
//		user2.setPassword(SecurityUtility.passwordEncoder().encode("didi"));
//		user2.setEmail("numugabodidier@gmail.com");
//		Role role2=new Role();
//		role2.setRoleid(1);
//		role2.setName("ROLE_USER");
//		userRoles.add(new UserRole(user2,role2));
//		userService.createUser(user2, userRoles);
//	}

}
