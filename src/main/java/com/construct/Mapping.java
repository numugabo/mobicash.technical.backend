package com.construct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.construct.Domain.Product;
import com.construct.Domain.User;
import com.construct.Domain.view.ProductViewModel;
import com.construct.Service.UserService;

@Component
public class Mapping {
	
	@Autowired
	private UserService userService;
	
	public ProductViewModel convertToProductViewModel(Product product) {
		
		ProductViewModel viewModel = new ProductViewModel();
		viewModel.setId(product.getId().toString());
		viewModel.setSku(product.getSku());
		viewModel.setUpc(product.getUpc());
		viewModel.setPname(product.getPname());
		viewModel.setTaxcode(product.getTaxcode());
		viewModel.setUserid(product.getUser().getId().toString());
		
		return viewModel;
	}
	
	public Product convertToProduct(ProductViewModel viewModel) {
		
		User user = userService.findById(Long.parseLong(viewModel.getUserid()));
		
		Product product = new Product(viewModel.getSku(), viewModel.getUpc(), viewModel.getPname(), viewModel.getTaxcode(), user);
		
		return product;
		
	}
}
