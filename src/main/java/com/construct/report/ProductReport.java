package com.construct.report;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import com.construct.Domain.Product;

public class ProductReport extends AbstractXlsView{

	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		response.setHeader("Content-Disposition", "attachment;filename=\"my-xls-file.xls\"");
		@SuppressWarnings("unchecked")
		List<Product> productList = (List<Product>) model.get("product");
		
		Sheet sheet = workbook.createSheet("Product Details");
		
		CellStyle style = workbook.createCellStyle();
	    Font font = workbook.createFont();
	    font.setFontName("Arial");
	    style.setFillForegroundColor(HSSFColor.BLUE.index);
	    style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	    font.setBold(true);
	    font.setColor(HSSFColor.WHITE.index);
	    font.setFontHeightInPoints((short) 12);
	    style.setFont(font);
	    
	    CellStyle style1 = workbook.createCellStyle();
	    font.setFontName("Arial");
	    style1.setFillForegroundColor(HSSFColor.GREEN.index);
	    style1.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	    font.setBold(true);
	    font.setColor(HSSFColor.WHITE.index);
	    font.setFontHeightInPoints((short) 12);
	    style1.setFont(font);
	    
	    CellStyle style2 = workbook.createCellStyle();
	    font.setFontName("Arial");
	    style2.setFillForegroundColor(HSSFColor.ORANGE.index);
	    style2.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	    font.setBold(true);
	    font.setColor(HSSFColor.WHITE.index);
	    font.setFontHeightInPoints((short) 12);
	    style2.setFont(font);
	    
	    Row header = sheet.createRow(0);
	    header.createCell(0).setCellValue("Product ID");
	    header.getCell(0).setCellStyle(style);
	    header.createCell(1).setCellValue("SKU");
	    header.getCell(1).setCellStyle(style);
	    header.createCell(2).setCellValue("UPC");
	    header.getCell(2).setCellStyle(style);
	    header.createCell(3).setCellValue("Product Name");
	    header.getCell(3).setCellStyle(style);
	    header.createCell(4).setCellValue("Tax code");
	    header.getCell(4).setCellStyle(style);
	    
	    int rowcount = 1;
	    for(Product pr: productList) {
	    	Row productRow =  sheet.createRow(rowcount++);
	    	productRow.createCell(0).setCellValue(pr.getId());
	    	productRow.createCell(1).setCellValue(pr.getSku());
	    	productRow.createCell(2).setCellValue(pr.getUpc());
	    	productRow.createCell(3).setCellValue(pr.getPname());
	    	productRow.createCell(4).setCellValue(pr.getTaxcode());
	    }
	    for(int i = 0; i < 5; i++) {
            sheet.autoSizeColumn(i);
        }
	}

}
