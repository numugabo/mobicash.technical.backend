package com.construct.Service;

import java.util.Set;

import com.construct.Domain.User;
import com.construct.Domain.UserRole;

public interface UserService {
	User createUser(User user, Set<UserRole> userRoles);
	User findByUsername(String username);
	User findByEmail(String email);
	User save(User user);
	User findById(Long id);
}
