package com.construct.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.construct.Domain.Product;
import com.construct.Domain.User;
import com.construct.Repository.ProductRepository;
import com.construct.Service.ProductService;
@Service
public class ProductServiceImpl implements ProductService{
	@Autowired
	private ProductRepository productRepository;

	@Override
	public Product save(Product product) {
		return productRepository.save(product);
	}

	@Override
	public Product findByUpc(String upc) {
		return productRepository.findByUpc(upc);
	}

	@Override
	public List<Product> findAllProductsByUser(User user) {
		return (List<Product>) productRepository.findAllProductsByUser(user);
	}

	@Override
	public List<Product> listAll() {
		return productRepository.findAll();
	}

	@Override
	public void deleteById(Long id) {
		productRepository.delete(id);
	}

	@Override
	public Product findByOne(Long id) {
		return productRepository.findOne(id);
	}
	
}
