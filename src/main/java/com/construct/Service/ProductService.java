package com.construct.Service;

import java.util.List;

import com.construct.Domain.Product;
import com.construct.Domain.User;

public interface ProductService {
	Product save(Product product);
	Product findByUpc(String upc);
	Product findByOne(Long id);
	List<Product> findAllProductsByUser(User user);
	List<Product> listAll();
	void deleteById(Long id);
}
