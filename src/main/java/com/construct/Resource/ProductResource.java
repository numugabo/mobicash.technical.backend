package com.construct.Resource;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.construct.Mapping;
import com.construct.Domain.Product;
import com.construct.Domain.User;
import com.construct.Domain.view.ProductViewModel;
import com.construct.Service.ProductService;
import com.construct.Service.UserService;
import com.construct.report.ProductReport;

@RestController
@RequestMapping("/product")
class ProductResource {

	@Autowired
	private ProductService productService;
	@Autowired
	private UserService userService;	
	@Autowired
	private Mapping mapping;

	@GetMapping("/all/{userId}")
	public List<ProductViewModel> AllProduct(@PathVariable String userId) {
		
		User user = userService.findById(Long.parseLong(userId));

		List<Product> products = productService.findAllProductsByUser(user);

		for (Product product : products) {
			System.out.println("id" + product.getId().toString());
		}

		List<ProductViewModel> productsViewModel = products.stream()
				.map(product -> mapping.convertToProductViewModel(product)).collect(Collectors.toList());

		return productsViewModel;
	}

	@GetMapping("/product/{upc}")
	public ProductViewModel findByUpc(@PathVariable String upc) {

		Product product = productService.findByUpc(upc);
		
		if (product == null) {
			throw new EntityNotFoundException();
		}

		ProductViewModel productViewModel = mapping.convertToProductViewModel(product);

		return productViewModel;
	}
	
	@GetMapping("/product/user/{userId}")
	public User findById(@PathVariable String userId) {

		User user = userService.findById(Long.parseLong(userId));
		
		return user;
	}
	
	@RequestMapping(value="/image", method=RequestMethod.POST)
	public ResponseEntity upload(@RequestParam("id") String id, HttpServletResponse response, HttpServletRequest request){
		try {
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			Iterator<String> it = multipartRequest.getFileNames();
			MultipartFile multipartFile = multipartRequest.getFile(it.next());
			String fileName = id+".png";
			
			
			byte[] bytes = multipartFile.getBytes();
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File("src/main/resources/static/image/"+fileName)));
			stream.write(bytes);
			stream.close();
			
			return new ResponseEntity("Upload Success!", HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity("Upload failed!", HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping(value ="/add")
	public Product save(@RequestBody ProductViewModel productViewModel, BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {
			throw new ValidationException();
		}
		
		Product product = mapping.convertToProduct(productViewModel);

		return productService.save(product);
	}
	
	@PostMapping(value ="/update")
	public ResponseEntity<?> update(@RequestBody ProductViewModel productViewModel, BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {
			throw new ValidationException();
		}
		
		Product product = mapping.convertToProduct(productViewModel);
		Product prod = productService.findByUpc(product.getUpc());
		prod.setPname(product.getPname());
		prod.setSku(product.getSku());
		prod.setTaxcode(product.getTaxcode());
		productService.save(prod);

		return new ResponseEntity<>("Product recorded well!", HttpStatus.OK);
	}

	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> delete(@PathVariable String id) throws Exception {
		try {
			System.out.println("ngiyi"+id);
			productService.deleteById(Long.parseLong(id));
			return new ResponseEntity<>("Product deleted well!", HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>("Failed deleted!", HttpStatus.BAD_REQUEST);
		}
		
//		String fileName = id+".png";
//		Files.delete(Paths.get("src/main/resources/static/image/"+fileName));
		
	}
	
	@GetMapping("/download")
	public ModelAndView reportofAllProducts() {
		ModelAndView modelView = new ModelAndView();
		modelView.addObject("product", productService.listAll());
		modelView.setView(new ProductReport());
		return modelView;
	}
	
//	@GetMapping("/download")
//	public ResponseEntity<?> downloadFile(String fileName, HttpServletResponse res) throws Exception {
//		try {
//			reportofAllProducts();
//			res.setHeader("Content-Disposition", "attachment; filename=" + fileName);
//			res.getOutputStream().write(contentOf(fileName));
//			return new ResponseEntity<>("Product deleted well!", HttpStatus.OK);
//		} catch (Exception e) {
//			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
//		}
//		
//	}
//	
//	private byte[] contentOf(String fileName) throws Exception{
//		return Files.readAllBytes(Paths.get(getClass().getClassLoader().getResource(fileName).toURI()));
//	}

}
