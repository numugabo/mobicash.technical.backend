package com.construct.Resource;

import java.security.Principal;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.construct.Domain.User;
import com.construct.Domain.view.Login;
import com.construct.Service.UserService;

@RestController
public class LoginResource {
	@Autowired
	private UserService userService;
	
	@GetMapping("/token")
//	public Map<String, Login> token(HttpSession session, HttpServletRequest request, Principal principal) {
	 public ResponseEntity<Login> token(HttpSession session, HttpServletRequest request, Principal principal) {
		System.out.println("Hello applicaton");
		 try {
			 System.out.println(request.getRemoteHost());

				String remoteHost = request.getRemoteHost();
				int portNumber = request.getRemotePort();
				System.out.println(request);
				System.out.println(remoteHost + ":" + portNumber);
				System.out.println(request.getRemoteAddr());
				User user = this.userService.findByUsername(principal.getName());
				System.out.println(user.getFirstName());
//				UserRole ur = userService.findUserRole(user.getId());
				Login log = new Login(user.getId().toString(), user.getFirstName(), user.getLastName(), session.getId());
//					return Collections.singletonMap("token", log);
					return new ResponseEntity<>(log, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			return null;
//			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("/api/checkSession")
	@Async("asyncExecutor")
	public CompletableFuture<ResponseEntity<String>> checkSession() 
	throws
	InterruptedException {
		Thread.sleep(1000);
		return CompletableFuture.completedFuture( new ResponseEntity<>("Session Active!", HttpStatus.OK));
	}

}
