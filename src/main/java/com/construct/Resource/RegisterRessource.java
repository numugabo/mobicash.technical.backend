package com.construct.Resource;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.construct.Config.SecurityUtility;
import com.construct.Domain.Role;
import com.construct.Domain.Sinup;
import com.construct.Domain.User;
import com.construct.Domain.UserRole;
import com.construct.Service.UserService;

@RestController
public class RegisterRessource {

	@Autowired
	private UserService userService;
	
	@PostMapping(value = "/registration")
	public ResponseEntity<?> sinup(@RequestBody Sinup sinup) {
		User user1 = new User();
		user1.setFirstName(sinup.getFirstname());
		user1.setLastName(sinup.getLastname());
		user1.setUsername(sinup.getEmail());
		user1.setPassword(SecurityUtility.passwordEncoder().encode(sinup.getPassword()));
		user1.setEmail(sinup.getPassword());
		Set<UserRole> userRoles = new HashSet<>();
		Role role1=new Role();
		role1.setRoleid(2);
		role1.setName("ROLE_GUEST");
		userRoles.add(new UserRole(user1,role1));
		userService.createUser(user1, userRoles);
		return new ResponseEntity<>("Registration well pecessed", HttpStatus.OK);
	}
	
	@PostMapping(value = "/product/updateUser/{userId}")
	public ResponseEntity<?> updateuser(@RequestBody Sinup sinup, @PathVariable String userId){
		try {
			User user = userService.findById(Long.parseLong(userId));
			user.setFirstName(sinup.getFirstname());
			user.setLastName(sinup.getLastname());
			user.setUsername(sinup.getEmail());
			userService.save(user);
			return new ResponseEntity<>("User updated well pecessed", HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		
		
	}
}
