package com.construct.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.construct.Domain.UserRole;


public interface UserRoleRepository extends JpaRepository<UserRole, Long>{
	@Query("from UserRole where user_id=?")
	UserRole findByUserRole(Long id);
}
