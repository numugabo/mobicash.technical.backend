package com.construct.Repository;

import org.springframework.data.repository.CrudRepository;

import com.construct.Domain.Role;


public interface RoleRepository extends CrudRepository<Role, Integer>{

}
