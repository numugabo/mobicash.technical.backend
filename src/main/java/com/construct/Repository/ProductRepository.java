package com.construct.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.construct.Domain.Product;
import com.construct.Domain.User;

public interface ProductRepository extends JpaRepository<Product,Long>{
	Product findByUpc(String upc);
	@Query("from Product where user = ?")
	List<Product> findAllProductsByUser(User user);
}
